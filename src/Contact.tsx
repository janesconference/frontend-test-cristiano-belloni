import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

export default function Contact({
  id,
  data
}: {
  id: string;
  data: ContactData | NotPresent;
}) {
  if (!data) return null;
  return (
    <Card>
      <CardContent>
        <Typography color="textPrimary" component="h3" gutterBottom>
          {data.name}
        </Typography>
        <Typography color="textSecondary">{data.email}</Typography>
      </CardContent>
      <CardActions>
        <Button component={Link} to={`/update/${id}`} size="small">
          Update
        </Button>
        <Button size="small" color="secondary">
          Remove
        </Button>
      </CardActions>
    </Card>
  );
}
