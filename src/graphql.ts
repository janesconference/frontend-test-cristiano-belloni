import ApolloClient, { gql } from "apollo-boost";
const client = new ApolloClient({
  uri: "http://localhost:3001"
});

export function getContacts() {
  return client
    .query({
      query: gql`
        {
          contacts {
            id
            name
            email
          }
        }
      `
    })
    .then(({ data }) => data.contacts);
}

export function getContact(context: AppContext): Promise<Contact> {
  const { contactId } = context;
  return client
    .query({
      query: gql`
      {contact(id: "${contactId}") { id name email }}
    `
    })
    .then(({ data }) => data.contact);
}

export function addContact(context: AppContext): Promise<Contact> {
  const { addDetails } = context;
  if (!addDetails) return Promise.reject();
  const { name, email } = addDetails;
  return client
    .mutate({
      mutation: gql`
      mutation {
        addContact(contact: { name: "${name}", email: "${email}" }) {
          id
        }
      }
    `
    })
    .then(({ data }) => data.addContact);
}

export function updateContact(context: AppContext): Promise<Contact> {
  const { updateDetails } = context;
  if (!updateDetails) return Promise.reject();
  const { id, name, email } = updateDetails;
  return client
    .mutate({
      mutation: gql`
      mutation {
        updateContact(contact: { id: "${id}", name: "${name}", email: "${email}" }) {
          id,
          name,
          email
        }
      }
    `
    })
    .then(({ data }) => data.updateContact);
}
