import React, { useState } from "react";
import { service } from "./services";

export default function Add({
  contactName = "",
  contactEmail = "",
  label = "Add",
  onSubmit
}: {
  contactName?: string;
  contactEmail?: string;
  label?: string;
  onSubmit?: (arg0: ContactData) => void;
}) {
  const [name, setName] = useState(contactName);
  const [email, setEmail] = useState(contactEmail);
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        if (!name || !email) return;
        setName("");
        setEmail("");
        if (onSubmit) onSubmit({ name, email });
        else {
          const event: AddContactEvent = { type: "ADD_CONTACT", name, email };
          service.send(event);
        }
      }}
    >
      <input
        value={name}
        onChange={e => setName(e.target.value)}
        placeholder="name"
      />
      <input
        value={email}
        onChange={e => setEmail(e.target.value)}
        placeholder="email"
        type="email"
      />
      <button>{label}</button>
    </form>
  );
}
