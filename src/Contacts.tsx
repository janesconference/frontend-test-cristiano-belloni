import React from "react";
import { Link } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";

export default function Contacts({ data }: { data: Contact[] }) {
  return (
    <List>
      {data.map(contact => (
        <ListItem
          button
          component={Link}
          to={`/contacts/${contact.id}`}
          key={contact.id}
          alignItems="flex-start"
        >
          <ListItemAvatar>
            <Avatar alt="Remy Sharp">{contact.name.slice(0, 1)}</Avatar>
          </ListItemAvatar>
          <ListItemText primary={contact.name} secondary={contact.email} />
        </ListItem>
      ))}
    </List>
  );
}
