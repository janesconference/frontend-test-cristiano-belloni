interface ContactData {
  name: string;
  email: string;
}
interface Contact extends ContactData {
  id: string;
}
type TParams = { id: string };
type NotPresent = null | undefined;

interface GenericAppEvent {
  type: string;
}

type FetchContactEvent = { type: "FETCH_CONTACT"; id: string };
type FetchContactEvents = { type: "FETCH_CONTACTS" };
type AddContactEvent = { type: "ADD_CONTACT"; name: string; email: string };
type ContactFetchedEvent = {
  type: "done.invoke.fetching-contact";
  data: Contact;
};

interface ContactsFetchedEvent extends GenericAppEvent {
  data: Contact[];
}
interface ContactAddedEvent extends GenericAppEvent {
  data: Contact;
}
interface ContactUpdatedEvent extends GenericAppEvent {
  data: Contact;
}

type UpdateContactEvent = {
  type: "UPDATE_CONTACT";
  id: string;
  name: string;
  email: string;
};

type AppEvent =
  | FetchContactEvent
  | FetchContactEvents
  | AddContactEvent
  | UpdateContactEvent
  | DoneContactEvent
  | ContactFetchedEvent
  | ContactsFetchedEvent
  | ContactAddedEvent
  | ContactUpdatedEvent;

interface AppContext {
  contactId: string | NotPresent;
  addDetails: ContactData | NotPresent;
  updateDetails: Contact | NotPresent;
  contactData: Contact | NotPresent;
  contactsData: Contact[];
  loading: boolean | NotPresent;
}
