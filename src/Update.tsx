import React from "react";
import Add from "./Add";
import { service } from "./services";

export default function Update({
  id,
  data
}: {
  id: string;
  data: ContactData | NotPresent;
}) {
  if (!data) return null;
  return (
    <Add
      contactName={data.name}
      contactEmail={data.email}
      label="Update"
      onSubmit={({ name, email }) => {
        const event: UpdateContactEvent = {
          type: "UPDATE_CONTACT",
          id,
          name,
          email
        };
        service.send(event);
      }}
    />
  );
}
