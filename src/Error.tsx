import React from "react";
import { Link } from "react-router-dom";

export default function Error() {
  return (
    <div>
      <p>Something wennt wrong!</p>
      <Link to="/">Go back home</Link>
    </div>
  );
}
