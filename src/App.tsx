import React from "react";
import { Router, Route, NavLink, Switch } from "react-router-dom";
import { useService } from "@xstate/react";
import { service, history } from "./services";
import Contacts from "./Contacts";
import Contact from "./Contact";
import Update from "./Update";
import Add from "./Add";
import Error from "./Error";

import "./App.css";

function App() {
  const [{ context }] = useService(service);
  if (!context) return null;
  if (context.loading) return <p>Loading...</p>;
  return (
    <div className="App">
      <header className="App-header">
        <h1>Contacts</h1>
        <Router history={history}>
          <div>
            <NavLink to="/contacts">Contacts</NavLink>
            <NavLink to="/add">Add contact</NavLink>
          </div>
          <Switch>
            <Route exact path="/">
              Home (links)
            </Route>
            <Route
              exact
              path="/contacts"
              render={() => <Contacts data={context.contactsData} />}
            />
            <Route exact path="/add" render={() => <Add />} />
            <Route
              exact
              path="/contacts/:id"
              render={routeProps => (
                <Contact
                  id={routeProps.match.params.id}
                  data={context.contactData}
                />
              )}
            />
            <Route
              exact
              path="/update/:id"
              render={routeProps => (
                <Update
                  id={routeProps.match.params.id}
                  data={context.contactData}
                />
              )}
            />
            <Route exact path="/error" render={() => <Error />} />
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
