import { Machine, assign, interpret } from "xstate";
import { createBrowserHistory, Location } from "history";
import { matchPath, match } from "react-router-dom";
import { getContacts, getContact, addContact, updateContact } from "./graphql";

interface AppStateSchema {
  states: {
    idle: {};
    fetching_contact: {};
    fetching_contacts: {};
    updating_contact: {};
    adding_contact: {};
  };
}

const machine = Machine<AppContext, AppStateSchema, AppEvent>({
  id: "contacts",
  initial: "idle",
  context: {
    contactId: "",
    addDetails: { name: "", email: "" },
    updateDetails: { name: "", email: "", id: "string" },
    contactData: { name: "", email: "", id: "string" },
    contactsData: [],
    loading: false
  },
  states: {
    idle: {
      on: {
        FETCH_CONTACT: {
          target: "fetching_contact",
          actions: [
            assign<AppContext, AppEvent>({ loading: () => true }),
            assign<AppContext, AppEvent>({
              contactId: (_: AppContext, event: AppEvent) => event.id
            })
          ]
        },
        FETCH_CONTACTS: {
          target: "fetching_contacts",
          actions: [
            assign<AppContext, AppEvent>({
              loading: true
            }),
            assign<AppContext, AppEvent>({
              contactId: null
            })
          ]
        },
        ADD_CONTACT: {
          target: "adding_contact",
          actions: [
            assign<AppContext, AppEvent>({
              loading: true
            }),
            assign<AppContext, AppEvent>({
              addDetails: (_: AppContext, event: AppEvent) => ({
                name: event.name,
                email: event.email
              })
            })
          ]
        },
        UPDATE_CONTACT: {
          target: "updating_contact",
          actions: [
            assign<AppContext, AppEvent>({
              loading: true
            }),
            assign<AppContext, AppEvent>({
              updateDetails: (_: AppContext, event: AppEvent) => ({
                id: event.id,
                name: event.name,
                email: event.email
              })
            })
          ]
        }
      }
    },
    fetching_contact: {
      invoke: {
        id: "fetching-contact",
        src: getContact,
        onDone: {
          target: "idle",
          actions: [
            assign<AppContext, AppEvent>({ loading: false }),
            assign<AppContext, ContactFetchedEvent>({
              contactData: (_: AppContext, event: ContactFetchedEvent) =>
                event.data
            })
          ]
        },
        onError: {
          target: "idle",
          actions: () => history.push(`/error`)
        }
      }
    },
    fetching_contacts: {
      invoke: {
        id: "fetching-contacts",
        src: getContacts,
        onDone: {
          target: "idle",
          actions: [
            assign<AppContext, AppEvent>({ loading: false }),
            assign<AppContext, ContactsFetchedEvent>({
              contactsData: (_: AppContext, event: ContactsFetchedEvent) =>
                event.data
            })
          ]
        },
        onError: {
          target: "idle",
          actions: () => history.push(`/error`)
        }
      }
    },
    adding_contact: {
      invoke: {
        id: "adding-contact",
        src: addContact,
        onDone: {
          target: "idle",
          actions: [
            assign<AppContext, AppEvent>({ loading: false }),
            assign<AppContext, ContactAddedEvent>({ addDetails: null }),
            (_: AppContext, event: ContactAddedEvent) =>
              history.push(`/contacts/${event.data.id}`)
          ]
        },
        onError: {
          target: "idle",
          actions: () => history.push(`/error`)
        }
      }
    },
    updating_contact: {
      invoke: {
        id: "updating-contact",
        src: updateContact,
        onDone: {
          target: "idle",
          actions: [
            assign<AppContext, AppEvent>({ loading: false }),
            assign<AppContext, AppEvent>({ updateDetails: null }),
            assign<AppContext, ContactUpdatedEvent>({
              contactData: (_: AppContext, event: ContactUpdatedEvent) =>
                event.data
            }),
            (_: AppContext, event: ContactUpdatedEvent) =>
              history.push(`/contacts/${event.data.id}`)
          ]
        },
        onError: {
          target: "idle",
          actions: () => history.push(`/error`)
        }
      }
    }
  }
});

export const service = interpret(machine);

service.onTransition(state => console.log("new state snapshot ->", state));

service.start();

// Location changes that generate actions are part of the business logic, not the view
const pathMap = [
  {
    path: "/contacts",
    handler: () => {
      service.send({ type: "FETCH_CONTACTS" });
    }
  },
  {
    path: "/contacts/:id",
    handler: (m: match<TParams>) => {
      service.send("FETCH_CONTACT", { id: m.params.id });
    }
  },
  {
    path: "/update/:id",
    handler: (m: match<TParams>) => {
      service.send("FETCH_CONTACT", { id: m.params.id });
    }
  }
];

export const history = createBrowserHistory({
  getUserConfirmation: () => false,
  basename:
    process.env.REACT_APP_ROUTER_BASE_URL || process.env.PUBLIC_URL || ""
});

history.listen(handleLocation);
handleLocation(history.location);

function handleLocation(location: Location) {
  pathMap.some(handlerConfig => {
    const { path, handler } = handlerConfig;
    const match = matchPath<TParams>(location.pathname, { path, exact: true });
    if (match) handler(match);
    return !!match;
  });
}
